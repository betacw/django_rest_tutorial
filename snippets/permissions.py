from rest_framework import permissions

class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    custom permissions to allow only users with permissions to edit a snippet
    """
    def has_object_permission(self, request, view, obj):
        """
        Read permissions always allowed
        :param request:
        :param view:
        :param obj:
        :return:
        """
        if request.method in permissions.SAFE_METHODS:
            return True
        return object.owner == request.user