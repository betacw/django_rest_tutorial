from rest_framework import serializers

from snippets.factory.comment import Comment


class CommentSerializer(serializers.Serializer):
    email = serializers.EmailField()
    content = serializers.CharField(max_length=200)
    created = serializers.DateTimeField()

    def create(self, validated_data):
        return Comment(**validated_data)

    def update(self,instance,validate_data):
        instance.email = validate_data.get('email',instance.email)
        instance.content = validate_data.get('content',instance.content)
        instance.create = validate_data.get('created',instance.created)
        return instance