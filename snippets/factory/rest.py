import requests,json



#python script for testing api's

class Rest:


    def __init__(self,uri):
        #class constructor, makes the url)
        # self.url="http://api.jkusdachurch.org/"+uri
        self.url="https://api.sendyit.com/v1/#"+uri
        # self.url = "http://localhost/jkusda.api" + uri
        self.type = False
        self.myresponse = dict()

    def set_payload(self,value):
        if 'headers' in value:
            self.headers=value['headers']
        if 'data' in value:
            self.payload=json.dumps(value['data'])

    def get_payload(self):
        return self.payload


    def prep_data(self,key=None,**data):
        """prepares data for posting to api
        """
        if key != None:
            head={'api-key': key,'Content-Type': 'application/json'}
        else:
            head={'Content-Type': 'application/json'}


        preparedata = {'headers': head, 'data': data}
        return preparedata

    def prep_helper(self,**val):
        return val

    def make_call(self):
        # print(self.payload)
        self.r=requests.post(self.url,self.payload,headers=self.headers)
        myresponse = (json.loads(self.r.text))
        # print(self.r)
        if myresponse['status']:
            self.type = True
            if 'message' in myresponse:
                self.myresponse = myresponse['message']
            elif 'data' in myresponse:
                self.myresponse = myresponse['data']
            else:
                self.myresponse=myresponse

        else:
            self.myresponse=myresponse

    def output_it(self):
        if self.type:
            return self.myresponse
        return "Error message: \t"+str(self.myresponse)


# #
# studget = Rest('assocget')  # test assocget, student, associate,malipo
# key = '123456ouaoidkjuidsxiudinjdsjnds'
# command='request'
# hadi=studget.prep_helper(to_name="KICC",to_lat=-1.28869,to_long=36.823363,to_description="")
# toka=studget.prep_helper(from_name="Sendy",from_lat=-1.300577,from_long=36.78183,from_description="")
# collector=studget.prep_helper(status=False,pay_method=0,amount=10)
# mengi=studget.prep_helper(pick_up_date="2016-04-20 12:12:12",collect_payment=collector)
# nani=studget.prep_helper(recepient_name='Clifford',recepient_phone='0706677565',recepient_email='betaclifford@gmail.com')
# data=studget.prep_helper(api_key='FmnjSYnn7Fe8uCRzzzza',api_username='mine',to=hadi,recepient=nani,delivery_details=mengi,note="Sample note",note_status=True,requests_type='quote')
# data=studget.prep_data(command=command, data=data)
# studget.set_payload(data)
# print((studget.get_payload()))
# studget.make_call()
# print(studget.output_it())
# # return HttpResponse(studget.output_it())
#
