from django.contrib.auth.models import User
from rest_framework import serializers

from snippets.factory.comment import Comment
from snippets.factory.posta import Posta
from snippets.models import Snippet


class SnippetSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    highlight = serializers.HyperlinkedIdentityField(view_name='snippet-highlight', format='html')

    class Meta:
        model = Snippet
        fields = ('url', 'code', 'linenos', 'language', 'style', 'owner', 'highlight')

class PostaSerializer(serializers.Serializer):
    email = serializers.EmailField()
    message = serializers.CharField(max_length=50)

    def create(self, validated_data):
        return Posta(**validated_data)

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.content = validated_data.get('message', instance.message)
        return instance

    # def save(self):
    #     email = self.validated_data['email']
    #     message = self.validated_data['message']



class UserSerializer(serializers.HyperlinkedModelSerializer):
    snippets = serializers.HyperlinkedRelatedField(many=True, view_name='snippet-detail', read_only=True)

    class Meta:
        model = User
        fields = ('url', 'username', 'snippets')



class CommentSerializer(serializers.Serializer):
    email = serializers.EmailField()
    content = serializers.CharField(max_length=200)
    created = serializers.DateTimeField()

    def create(self, validated_data):
        return Comment(**validated_data)

    def update(self,instance,validate_data):
        instance.email = validate_data.get('email',instance.email)
        instance.content = validate_data.get('content',instance.content)
        instance.create = validate_data.get('created',instance.created)
        return instance
