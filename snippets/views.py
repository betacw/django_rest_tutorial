from django.contrib.auth.models import User
from django.http import HttpResponse
from rest_framework import permissions
from rest_framework import renderers
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.views import APIView
from snippets.models import Snippet
from snippets.permissions import IsOwnerOrReadOnly
from snippets.serializers import SnippetSerializer, UserSerializer, PostaSerializer
from rest_framework.response import Response
from snippets.factory.posta import Posta


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Automatically provides list and detail functions
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class SnippetViewSet(viewsets.ModelViewSet):
    """
    provides list ,create,update,destroy actions and an optional highlight action
    """
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        snippet = self.get_object()
        return Response(snippet.highlighted)

    def testit(self):
        from snippets.factory.rest import Rest
        studget = Rest('assocget')  # test assocget, student, associate,malipo
        key = '123456ouaoidkjuidsxiudinjdsjnds'
        studget.prep_data(key, command='request', values='yes')
        studget.make_call()
        return Response(studget.output_it())

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


def apitest(request):
    from snippets.factory.rest import Rest
    # renderer_classes = renderers.StaticHTMLRenderer
    # # the api parameters are defined in a list of comma separated values as shown in the function call to prep_data, prep_data handles the rest

    studget = Rest('assocget')  # test assocget, student, associate,malipo
    key = '123456ouaoidkjuidsxiudinjdsjnds'
    command = 'request'
    hadi = studget.prep_helper(to_name="KICC", to_lat=-1.28869, to_long=36.823363, to_description="")
    toka = studget.prep_helper(from_name="Sendy", from_lat=-1.300577, from_long=36.78183, from_description="")
    collector = studget.prep_helper(status=False, pay_method=0, amount=10)
    mengi = studget.prep_helper(pick_up_date="2016-04-20 12:12:12", collect_payment=collector)
    nani = studget.prep_helper(recepient_name='Clifford', recepient_phone='0706677565',
                               recepient_email='betaclifford@gmail.com')
    data = studget.prep_helper(api_key='FmnjSYnn7Fe8uCRzzzza', api_username='mine', to=hadi, recepient=nani,
                               delivery_details=mengi, note="Sample note", note_status=True, requests_type='quote')
    data = studget.prep_data(command=command, data=data)
    studget.set_payload(data)
    # print((studget.get_payload()))
    studget.make_call()
    # print(studget.output_it())
    return HttpResponse(str(studget.output_it()))


class ApiTest(APIView):

    def get(self,request,format=None):
        from snippets.factory.rest import Rest
        # renderer_classes = renderers.StaticHTMLRenderer
        # # the api parameters are defined in a list of comma separated values as shown in the function call to prep_data, prep_data handles the rest

        studget = Rest('assocget')  # test assocget, student, associate,malipo
        key = '123456ouaoidkjuidsxiudinjdsjnds'
        command = 'request'
        hadi = studget.prep_helper(to_name="KICC", to_lat=-1.28869, to_long=36.823363, to_description="")
        toka = studget.prep_helper(from_name="Sendy", from_lat=-1.300577, from_long=36.78183, from_description="")
        collector = studget.prep_helper(status=False, pay_method=0, amount=10)
        mengi = studget.prep_helper(pick_up_date="2016-04-20 12:12:12", collect_payment=collector)
        nani = studget.prep_helper(recepient_name='Clifford', recepient_phone='0706677565',
                                   recepient_email='betaclifford@gmail.com')
        data = studget.prep_helper(api_key='FmnjSYnn7Fe8uCRzzzza', api_username='mine', to=hadi, recepient=nani,
                                   delivery_details=mengi, note="Sample note", note_status=True, requests_type='quote')
        data = studget.prep_data(key=key,command=command, data=data)
        studget.set_payload(data)
        # print((studget.get_payload()))
        studget.make_call()
        # print(studget.output_it())
        return Response(studget.output_it())
        # pass
    def post(self,request,format=None):
        req=Posta(request.data)
        serializer = PostaSerializer(req)
        serializer = PostaSerializer(data=serializer.data)
        if serializer.is_valid():
            return Response(serializer.data)
        return Response(serializer.errors)

        # # return Response(serializer.data)
        # json = renderers.JSONRenderer().render(serializer.data)
        # from django.utils.six import BytesIO
        # from rest_framework.parsers import JSONParser
        # stream = BytesIO(json)
        # data = JSONParser().parse(stream)
        # serializer=PostaSerializer(data=data)
        #
        # if serializer.is_valid():
        #     return Response(serializer.validated_data)
        # # return Response(serializer.errors)



